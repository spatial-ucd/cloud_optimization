# Cloud Optimization

The goal of these scripts is to batch convert geotiffs to cloud optimized geotiffs (COGS).

## Scripts

1. Creates a catalog of tiff files for conversion
2. Converts the images in the catalog, creating a mirrored folder structure in the destination
3. Chooses a random sample of images to validate

## Notes

The scripts have been designed to be compatible with Slurm compute clusters.

Example calls:
```
# Create Catalog
sbatch -p med -t 5 --job-name=NAIPcat ~/computing/clusterPy3.sh ~/cloud_optimization/1_create-catalog.py /share/spatial02/library/public/ca/imagery/imagery/naip/2016/ca_60cm_2016/

# Test conversion
sbatch -p med -t 20 --array=1 --job-name=2COG ~/computing/clusterPy3.sh ~/cloud_optimization/2_convert2cog.py /share/spatial02/library/public/ca/imagery/imagery/naip/2016/ca_60cm_2016/ /share/spatial03/library/public/ca/imagery/imagery/naip/2016/ca_60cm_2016/
```